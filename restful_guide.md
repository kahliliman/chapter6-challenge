#==================================#
#==      RESTFUL API GUIDE       ==#
#==================================#

url                       method    type    description
/                         GET       views   Get the landing page
/game                     GET       views   Get the game page
/game/history             GET       views   Get the history page
/api/v1/game/session      POST      api     Init user, bot, and game. Create new session and new round and store session data
/api/v1/game/start        POST      api     Process the choice and return game result. Store results data 
/api/v1/game/refresh      POST      api     Reset game result and player choices. Create new round.
/api/v1/game/data         GET       api     Get historical data