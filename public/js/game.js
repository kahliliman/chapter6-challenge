/* eslint-disable no-restricted-syntax */
/* eslint-disable no-shadow */
/* eslint-env browser */

let sessionId
let roundId
let gameRound
let player1Name
let player2Name
let player1Choice
let player2Choice
let gameResult

const roundNumberText = document.querySelector('.round__number')
const versusText = document.querySelector('.divider__versus')
const statusWinText = document.querySelector('.divider__win')
const statusLoseText = document.querySelector('.divider__lose')
const statusDrawText = document.querySelector('.divider__draw')
const refreshBtn = document.querySelector('.refresh__btn')

// Utils

const gameMessage = () => {
  console.log(`-- ${player1Name} chooses ${player1Choice}`)
  console.log(`-- ${player2Name} chooses ${player2Choice}`)
  if (gameResult === 'draw') {
    console.log('It\'s a draw!'.toUpperCase())
  } else if (gameResult === 'win') {
    console.log(`${player1Name} wins this round!`.toUpperCase())
  } else if (gameResult === 'lose') {
    console.log(`${player2Name} wins this round!`.toUpperCase())
  }
  console.log('_________________________')
}

const showResults = () => {
  const userItems = document.getElementsByClassName('user__item')
  for (const item of userItems) {
    item.classList.remove('available__item')
  }

  versusText.classList.add('hidden')
  if (gameResult === 'win') {
    statusWinText.classList.remove('hidden')
    document.getElementsByClassName(`${player1Choice} user__item`)[0].classList.add('item__win')
    document.getElementsByClassName(`${player2Choice} bot__item`)[0].classList.add('item__lose')
  } else if (gameResult === 'lose') {
    statusLoseText.classList.remove('hidden')
    document.getElementsByClassName(`${player1Choice} user__item`)[0].classList.add('item__lose')
    document.getElementsByClassName(`${player2Choice} bot__item`)[0].classList.add('item__win')
  } else {
    statusDrawText.classList.remove('hidden')
    document.getElementsByClassName(`${player1Choice} user__item`)[0].classList.add('item__draw')
    document.getElementsByClassName(`${player2Choice} bot__item`)[0].classList.add('item__draw')
  }
  refreshBtn.classList.remove('hidden')
}

const clearResults = () => {
  const userItems = document.getElementsByClassName('user__item')
  const allItems = document.getElementsByClassName('item')

  for (const item of userItems) {
    item.classList.add('available__item')
  }

  for (const item of allItems) {
    item.classList.remove('item__win', 'item__lose', 'item__draw')
  }

  versusText.classList.remove('hidden')
  statusWinText.classList.add('hidden')
  statusLoseText.classList.add('hidden')
  statusDrawText.classList.add('hidden')
  refreshBtn.classList.add('hidden')
}

const postData = async (url = '', data = {}) => {
  const response = await fetch(url, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(data),
  })
  return response.json()
}

const patchData = async (url = '', data = {}) => {
  const response = await fetch(url, {
    method: 'PATCH',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(data),
  })
  return response.json()
}

const getData = async (url = '') => {
  const response = await fetch(url)
  return response.json()
}

const createSession = async () => {
  await postData('/api/v1/game/session/create')
    .then((response) => {
      // console.log(response)
      sessionId = response.id
      player1Name = response.player1Name
      player2Name = response.player2Name
      gameRound = response.gameRound
    })
  // console.log('Created!')
}

const createRound = async () => {
  await postData('/api/v1/game/round/create', { sessionId, gameRound })
    .then((response) => {
      // console.log(response)
      roundId = response.id
    })
  // console.log('Created!')
}

const updateRound = async () => {
  await patchData(`/api/v1/game/round/${roundId}`, { gameResult, player1Choice, player2Choice })
    .then((response) => {
      // console.log(response)
    })
  // console.log('Created!')
}

// POST Start
const startGame = async (item) => {
  await postData('/api/v1/game/start', { data: 'start', choice: item })
    .then((response) => {
      // console.log(response)
      player1Choice = response.player1Choice
      player2Choice = response.player2Choice
      gameResult = response.result
    })
  // console.log('Game started!')
}

// POST log Result

const logData = async () => {
  await postData('api/v1/game/log', {
    data: 'log',
    round: gameRound,
    result: gameResult,
    choice1: player1Choice,
    choice2: player2Choice,
  })
    .then((response) => {
      // console.log(response)
    })
  // console.log('Data Logged')
}

// POST Refresh
const refreshGame = async () => {
  await postData('/api/v1/game/refresh', { data: 'refresh' })
    .then((response) => {
      // console.log(response)
      player1Choice = response.player1Choice
      player2Choice = response.player2Choice
      gameResult = response.result
      gameRound = response.round
    })
  // console.log('Refreshed!')
}

// GET Historical Data

const getHistoricalData = async () => {
  await getData('/api/v1/game/history')
    .then((response) => {
      console.log(response)
    })
  console.log('Refreshed!')
}

// DOM

window.onload = async () => {
  await createSession()
  await createRound()
  roundNumberText.textContent = gameRound
  console.log('_________________________')
  console.log(`ROUND ${gameRound}`)
}

document.getElementById('rock').addEventListener('click', async () => {
  if (!gameResult) {
    await startGame('rock')
    showResults()
    gameMessage()
    updateRound()
    // logData()
  }
})

document.getElementById('paper').addEventListener('click', async () => {
  if (!gameResult) {
    await startGame('paper')
    showResults()
    gameMessage()
    updateRound()
    // logData()
  }
})

document.getElementById('scissors').addEventListener('click', async () => {
  if (!gameResult) {
    await startGame('scissors')
    showResults()
    gameMessage()
    updateRound()
    // logData()
  }
})

document.getElementById('refresh').addEventListener('click', async () => {
  clearResults()
  await refreshGame()
  await createRound()
  roundNumberText.textContent = gameRound
  console.log(`ROUND ${gameRound}`)
})
