/* eslint-disable no-useless-constructor */
import Player from './Player'

class User extends Player {
  constructor(name) {
    super(name)
  }
}

export default User
