/* eslint-disable no-restricted-syntax */
/* eslint-env browser */
import User from '../entities/User'
import Comp from '../entities/Comp'

class GameEngine {
  constructor() {
    this.player1 = new User('User')
    this.player2 = new Comp('Comp')
    this.itemChoices = ['rock', 'paper', 'scissors']
    this.result = null
    this.gameRound = 1
  }

  initGame = () => {
    this.player1.clearChoice()
    this.player2.clearChoice()
    this.result = null
    this.gameRound = 1
  }

  refreshGame = () => {
    this.player1.clearChoice()
    this.player2.clearChoice()
    this.incrementGameRound()
    this.result = null
  }

  beginGame(choice) {
    this.player1.setChoice(choice)
    const player1Choice = this.player1.getChoice()

    this.player2.randomizeChoice(this.itemChoices)
    const player2Choice = this.player2.getChoice()

    if (player1Choice === player2Choice) {
      this.result = 'draw'
    } else if ((player1Choice === 'rock' && player2Choice === 'scissors') || (player1Choice === 'paper' && player2Choice === 'rock') || (player1Choice === 'scissors' && player2Choice === 'paper')) {
      this.result = 'win'
    } else if ((player1Choice === 'rock' && player2Choice === 'paper') || (player1Choice === 'paper' && player2Choice === 'scissors') || (player1Choice === 'scissors' && player2Choice === 'rock')) {
      this.result = 'lose'
    }
  }

  // setter

  setResult = (result) => {
    this.result = result
  }

  incrementGameRound = () => {
    this.gameRound += 1
    console.log('increment happened')
  }

  // getter

  getResult = () => this.result

  getGameRound = () => this.gameRound

  getPlayer1 = () => this.player1

  getPlayer2 = () => this.player2
}

const game = new GameEngine()

// console.log(game.getGameRound())
// console.log(game.refreshGame())
// console.log(game.getGameRound())
// console.log(game.refreshGame())
// console.log(game.getGameRound())
// console.log(game.refreshGame())
// console.log(game.getGameRound())
// console.log(game.refreshGame())
// console.log(game.getGameRound())

export default game
