import express from 'express'
import bodyParser from 'body-parser'
import views from './routes/views'
import api from './routes/api'
import user from './routes/user'
import db from './models'

require('dotenv').config()

// Test DB Connection

db.sequelize.authenticate()
  .then(() => console.log('Database connected...'))
  .catch((err) => console.log(`Error: ${err}`))

const app = express()

app.use(bodyParser.urlencoded({ extended: false }))

app.use(express.static('public'))
app.use(express.json())

app.set('view engine', 'ejs')
app.set('views', 'src/views')

// VIEWS

app.use('/', views)

// API
app.use('/api/v1/game', api)
app.use('/api/v1/user', user)

app.listen(3000, () => console.log(`Listening on ${process.env.APP_URL}`))
