module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('Session', [
      {
        id: 'f43c6b55-1517-4cae-8318-4fa317a125d0',
        playerId: '916bd93b-7eb2-441a-9600-58671b0026df',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id: '261d4011-d62e-450b-beb2-f2d175addb51',
        playerId: '916bd93b-7eb2-441a-9600-58671b0026df',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ], {});
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('Session', null, {});
  },
};
