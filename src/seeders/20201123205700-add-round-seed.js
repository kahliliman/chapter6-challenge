module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('Round', [
      {
        id: '7de99034-5273-4a8c-82c7-2356df973ea9',
        number: 1,
        result: 'win',
        player1Choice: 'rock',
        player2Choice: 'scissors',
        sessionId: 'f43c6b55-1517-4cae-8318-4fa317a125d0',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id: '48d9fee2-8bc7-4ae7-b7e7-063fb27e9538',
        number: 2,
        result: 'lose',
        player1Choice: 'rock',
        player2Choice: 'paper',
        sessionId: 'f43c6b55-1517-4cae-8318-4fa317a125d0',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id: '57e2f575-8458-4291-a7b2-5cdb23287ecf',
        number: 3,
        result: 'draw',
        player1Choice: 'rock',
        player2Choice: 'rock',
        sessionId: 'f43c6b55-1517-4cae-8318-4fa317a125d0',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id: '4d7e7326-0494-4325-a989-bf91d8c75a7f',
        number: 1,
        result: 'lose',
        player1Choice: 'paper',
        player2Choice: 'scissors',
        sessionId: '261d4011-d62e-450b-beb2-f2d175addb51',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id: 'f2305748-88a6-478d-84c6-b2af0355ed06',
        number: 2,
        result: 'win',
        player1Choice: 'paper',
        player2Choice: 'rock',
        sessionId: '261d4011-d62e-450b-beb2-f2d175addb51',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id: '2e00ad5d-bcef-494c-91e4-c53944006ead',
        number: 3,
        result: 'draw',
        player1Choice: 'paper',
        player2Choice: 'paper',
        sessionId: '261d4011-d62e-450b-beb2-f2d175addb51',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ], {});
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('Round', null, {});
  },
};
