import url from 'url';
import { Op } from 'sequelize'
import game from '../../engine'
import { Session, User, Round } from '../../models'

class GameController {
  static createSession = async (req, res) => {
    const urlQuery = url.parse(req.headers.referer).search

    const { username } = url.parse(urlQuery, true).query
    const userId = await User.findOne({
      where: { username },
    }).then((data) => {
      const { id } = data
      return id
    })

    game.initGame()

    return Session.create({
      playerId: userId,
      createdAt: new Date(),
      updatedAt: new Date(),
    }).then(
      (session) => {
        res.status(201).json({
          id: session.id,
          session: true,
          player1Name: game.getPlayer1().getName(),
          player2Name: game.getPlayer2().getName(),
          gameRound: game.getGameRound(),
        })
      },
    )
      .catch(
        () => console.log('Error'),
      )
  }

  static createRound = async (req, res) => {
    const { sessionId, gameRound } = req.body

    Round.create({
      number: gameRound,
      sessionId,
      createdAt: new Date(),
      updatedAt: new Date(),
    }).then(
      (data) => {
        res.status(201).json({
          id: data.id,
        })
      },
    )
      .catch(
        () => console.log('Error'),
      )
  }

  static updateRound = async (req, res) => {
    const { id } = req.params

    return Round.findOne({
      where: { id },
    }).then(
      (round) => {
        if (!round) return res.status(404).json({ message: 'Not found' })

        const {
          gameResult,
          player1Choice,
          player2Choice,
        } = req.body

        return round.update(
          { result: gameResult, player1Choice, player2Choice },
        ).then(
          (updated) => res.status(200).json({ ...updated.dataValues }),
        )
      },
    ).catch(
      (e) => console.log(e),
    )
  }

  static startGame = (req, res) => {
    if (req.body.data !== 'start') {
      res.status(400).send({ error: true, message: 'bad input' })
    }
    game.beginGame(req.body.choice)
    res.status(200).json({
      start: true,
      player1Choice: game.getPlayer1().getChoice(),
      player2Choice: game.getPlayer2().getChoice(),
      result: game.getResult(),
    })
  }

  static refreshGame = (req, res) => {
    if (req.body.data !== 'refresh') {
      res.status(400).send({ error: true, message: 'bad input' })
    }
    game.refreshGame()
    res.status(200).json({
      refresh: true,
      player1Choice: game.getPlayer1().getChoice(),
      player2Choice: game.getPlayer2().getChoice(),
      result: game.getResult(),
      round: game.getGameRound(),
    })
  }

  static getHistoryData = (req, res) => {
    Round.findAll({
      attributes: [['number', 'roundNumber'], 'result', 'player1Choice', 'player2Choice'],
      order: [
        ['createdAt', 'asc'],
      ],
      include: [
        {
          model: Session,
          as: 'Session',
          attributes: ['id', 'createdAt'],
        },
      ],
    }).then(
      (rounds) => res.status(200).json(rounds),
    ).catch(
      (e) => console.log(e),
    )
  }
}

export default GameController
