import { User } from '../../models'

class UserController {
  static authUser = (req, res) => {
    const { username, password } = req.body

    User.findOne({
      where: { username, password },
    })
      .then((data) => {
        if (data) {
          return res.redirect(`${process.env.APP_URL}/game?username=${username}`)
        }
        return res.status(400).json({ message: 'wrong username/password' })
      })
      .catch((e) => console.log(e))
  }

  static createUser = (req, res) => {
    const { username, password } = req.body

    User.findOne({
      where: { username },
    })
      .then((data) => {
        if (!data) {
          return User.create({
            username,
            password,
            createdAt: new Date(),
            updatedAt: new Date(),
          }).then(
            () => res.redirect(`${process.env.APP_URL}/game?username=${username}`),
          ).catch(
            (e) => console.log(e),
          )
        }
        return res.status(400).json({ message: 'username already exist' })
      })
  }
}

export default UserController
