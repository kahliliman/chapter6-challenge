import game from '../../engine'

class ViewController {
  static getLandingPage = (req, res) => {
    res.status(200).render('index')
  }

  static getGamePage = (req, res) => {
    res.status(200).render('game', { data: game, query: req.query })
  }

  static getHistoryPage = (req, res) => {
    res.status(200).render('game/history')
  }

  static getLoginPage = (req, res) => {
    res.status(200).render('auth/login')
  }

  static getSignupPage = (req, res) => {
    res.status(200).render('auth/signup')
  }
}

export default ViewController
