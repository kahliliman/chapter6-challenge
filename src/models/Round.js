import { Model } from 'sequelize'

module.exports = (sequelize, DataTypes) => {
  class Round extends Model {
    static associate(models) {
      const { Session } = models

      Round.belongsTo(Session, { foreignKey: 'sessionId' })
    }
  }

  Round.init({
    id: {
      primaryKey: true,
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
    },
    number: DataTypes.INTEGER,
    result: {
      type: DataTypes.ENUM,
      values: ['win', 'lose', 'draw'],
    },
    player1Choice: {
      type: DataTypes.ENUM,
      values: ['rock', 'paper', 'scissors'],
    },
    player2Choice: {
      type: DataTypes.ENUM,
      values: ['rock', 'paper', 'scissors'],
    },
    sessionId: DataTypes.UUID,
  }, {
    sequelize,
    modelName: 'Round',
    timestamps: true,
  })

  return Round
}
