import { Model } from 'sequelize'

module.exports = (sequelize, DataTypes) => {
  class Session extends Model {
    static associate(models) {
      const { Round, User } = models
      Session.hasMany(Round, { foreignKey: 'id' })
      Session.belongsTo(User, { foreignKey: 'playerId' })
    }
  }

  Session.init({
    id: {
      primaryKey: true,
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
    },
    playerId: {
      type: DataTypes.UUID,
    },
  }, {
    sequelize,
    modelName: 'Session',
    timestamps: true,
  })

  return Session
}
