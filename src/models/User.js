import { Model } from 'sequelize'

module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    static associate(models) {
      const { Session } = models
      User.hasMany(Session, { foreignKey: 'id' })
    }
  }

  User.init({
    id: {
      primaryKey: true,
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
    },
    username: DataTypes.STRING,
    password: DataTypes.STRING,
  }, {
    sequelize,
    modelName: 'User',
    timestamps: true,
  })

  return User
}
