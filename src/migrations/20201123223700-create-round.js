module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Round', {
      id: {
        type: Sequelize.UUID,
        primaryKey: true,
        allowNull: false,
        unique: true,
      },
      number: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      result: {
        type: Sequelize.ENUM,
        values: ['win', 'lose', 'draw'],
      },
      player1Choice: {
        type: Sequelize.ENUM,
        values: ['rock', 'paper', 'scissors'],
      },
      player2Choice: {
        type: Sequelize.ENUM,
        values: ['rock', 'paper', 'scissors'],
      },
      sessionId: {
        type: Sequelize.UUID,
        allowNull: false,
        references: {
          model: 'Session',
          key: 'id',
        },
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    })
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Round')
  },
}
