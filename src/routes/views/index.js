import express from 'express'
import ViewController from '../../controllers/ViewController'

const router = express.Router()

router.get('/', ViewController.getLandingPage)

router.get('/game', ViewController.getGamePage)

router.get('/game/history', ViewController.getHistoryPage)

router.get('/auth/login', ViewController.getLoginPage)

router.get('/auth/signup', ViewController.getSignupPage)

export default router
