import express from 'express'
import GameController from '../../controllers/GameController'

const router = express.Router()

router.post('/session/create', GameController.createSession)

router.post('/round/create', GameController.createRound)

router.patch('/round/:id', GameController.updateRound)

router.post('/start', GameController.startGame)

// router.post('/log', GameController.logGameResult)

router.post('/refresh', GameController.refreshGame)

router.get('/data', GameController.getHistoryData)

export default router
