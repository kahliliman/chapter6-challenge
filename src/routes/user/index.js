import express from 'express'
import ApiController from '../../controllers/UserController'

const router = express.Router()

router.post('/create', ApiController.createUser)
router.post('/auth', ApiController.authUser)

export default router
